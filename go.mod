module gitlab.com/godeater/swaylock_imagechanger

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/h2non/filetype v1.0.12
	github.com/mitchellh/go-homedir v1.1.0
)
