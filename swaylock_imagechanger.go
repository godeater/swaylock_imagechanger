package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/h2non/filetype"
	"github.com/mitchellh/go-homedir"
	//"github.com/davecgh/go-spew/spew"
)

type config struct {
	LastImage string   `json:"LastImage"`
	ImageDir  string   `json:"ImageDir"`
	ImageList []string `json:"ImageList"`
}

var (
	currentConfig config
	configfile    string
)

func main() {
	init := flag.Bool("init", false, "Whether to initialize the list of images or not")
	imageDir := flag.String("imagedir", "WallPaper", "The directory containing images to loop through")
	flag.Parse()

	if *init {
		initConfig(true)
		buildImageList(*imageDir)
	} else {
		initConfig(false)
		loadConfig()
		//spew.Dump(currentConfig)
		newImage := currentConfig.NextImage()
		imageMessage := filepath.Join(currentConfig.ImageDir, newImage)
		fmt.Printf("%s\n", imageMessage)
	}

}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func (c config) NextImage() string {
	var currentPosition int
	length := len(currentConfig.ImageList)
	if length != 0 {
		length = length - 1
	}
	//log.Printf("Current length of ImageList: %d\n", length)

	for i, n := range currentConfig.ImageList {
		if currentConfig.LastImage == n {
			currentPosition = i
		}
	}
	//log.Printf("Current position of %s : %d\n", currentConfig.LastImage, currentPosition)

	if currentPosition != length {
		currentPosition++
	} else {
		currentPosition = 0
	}
	currentConfig.LastImage = currentConfig.ImageList[currentPosition]
	saveConfig()
	return currentConfig.LastImage
}

func initConfig(init bool) {
	homeDir, err := homedir.Dir()
	check(err)
	configDir := filepath.Join(homeDir, ".config/swaylock_imagechanger")
	check(err)
	configDirDetail, err := os.Stat(configDir)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(configDir, 0755)
		check(errDir)
	}

	if configDirDetail.Mode().IsRegular() {
		log.Fatal(".config/swaylock_imagechanger already exists as a file")
	}

	configfile = filepath.Join(configDir, "config")
	_, err = os.Stat(configfile)

	if init {
		return
	}
	if os.IsNotExist(err) {
		log.Fatal("Please re-run init")
	}
}

func loadConfig() {
	configContent, err := ioutil.ReadFile(configfile)
	check(err)
	err = json.Unmarshal(configContent, &currentConfig)
	check(err)
}

func saveConfig() {
	jsonConfig, err := json.MarshalIndent(currentConfig, "", "\t")
	check(err)
	err = ioutil.WriteFile(configfile, jsonConfig, 0644)
	check(err)
}

func buildImageList(dirname string) {
	images, err := ioutil.ReadDir(dirname)
	if err != nil {
		log.Fatal(err)
	}
	currentConfig.ImageDir = dirname

	for _, file := range images {
		pathToImage := filepath.Join(dirname, file.Name())
		buf, err := ioutil.ReadFile(pathToImage)
		check(err)
		if filetype.IsImage(buf) {
			//log.Printf("%s is an image\n",file.Name())
			currentConfig.ImageList = append(currentConfig.ImageList, file.Name())
		} else {
			log.Printf("%s is NOT an image\n", file.Name())
		}
	}

	currentConfig.LastImage = currentConfig.ImageList[0]
	//	jsonConfig, err := json.MarshalIndent(currentConfig,"","\t")
	//	if err != nil {
	//		log.Fatal("Can't serialize the image list")
	//	}
	//	err = ioutil.WriteFile("config",jsonConfig,0644)
	//	if err != nil {
	//		log.Fatal("Can't write to imageList file")
	//	}
	saveConfig()
}
