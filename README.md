# swaylock_imagechanger

A utility to pass a new image to swaylock whenever it's called.

I'm painfully aware that there's probably a much easier and more elegant way to do this, but it was a neat little programming challenge for a quiet weekend :)

## Instructions

At some point I might turn this into an AUR package for Arch (my distro of choice), but until then you should be able to install it using 

```bash
go get gitlab.com/godeater/swaylock_imagechanger
```

Once installed, you need to initialize its config file, which will contain the list of images you want to rotate through, and the position in that list that the utility last used.

```bash
swaylock_imagechanger --init --imagedir ~/Pictures/Wallpaper
```

Finally, you'll want to alter the line in your sway config file which runs swaylock to something like this

```
set $lock exec swaylock -c 111111 -i $(swaylock_imagechanger)
```

The $(swaylock_imagechanger) is the key part - this will be run whenever swaylock is called, and will return the path to an image in the directory you specified for --imagedir in the initialization command.
